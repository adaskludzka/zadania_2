package zad.algorytmy.wstep;

import java.util.Scanner;

public class zad21 {
    //Napisz metodę, która zwraca maksymalną długość 2 stringów.
    //maxStringLength(“aaaa”,”sad”) zwróci 4.
    //maxStringLength(“aaksadui”,”aaa”) zwróci 8.

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String a = sc.next();
        String b = sc.next();
        char[] c;
        char[] d;
        c=a.toCharArray();
        d=b.toCharArray();
        maxStringLength(c,d);

    }

    private static void maxStringLength(char[] c, char[] d){
        if(c.length > d.length){
            System.out.println(c.length);
        }else{
            System.out.println(d.length);
        }

    }
}
