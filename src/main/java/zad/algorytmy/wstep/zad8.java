package zad.algorytmy.wstep;

import java.util.Arrays;

public class zad8 {
    //Napisz metodę, która zwraca posortowaną tablicę liczb.
    //mySort([4,1,9,15]) = [1,4,9,15]

    public static void main(String[] args) {
        int[] mySort = {4, 1, 9, 15};
        arraySort(mySort);

    }

    private static void arraySort(int[] mySort) {
        System.out.println("Przed: " + Arrays.toString(mySort));
        boolean swappedSomething = true;
        while (swappedSomething) {
            swappedSomething = false;
            for (int i = 0; i < mySort.length - 1; i++) {
                if (mySort[i] > mySort[i + 1]) {
                    swappedSomething = true;
                    int temp = mySort[i];
                    mySort[i] = mySort[i + 1];
                    mySort[i + 1] = temp;
                }
            }
        }

        System.out.println("\nPo sortowaniu: " + Arrays.toString(mySort));
    }
}
