package zad.algorytmy.wstep;

import java.util.Scanner;

public class zad19 {
    //Napisz funkcję, która stwierdza, czy zadana jako parametr liczba całkowita jest
    //kwadratem pewnej liczby całkowitej. Liczby będące kwadratami liczb całkowitych
    //to 1, 4, 9, 16 itd. Wartością funkcji ma być prawda, jeśli liczba spełnia warunek oraz
    //fałsz w przeciwnym wypadku.

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        boolean isSquared = true;

        isNumberSquared(a,b,isSquared);

    }

    private static void isNumberSquared(int a,int b,boolean isSquared){
        if(a*a==b){
            System.out.println(isSquared);
        }else{
            System.out.println(!isSquared);
        }
    }
}
