package zad.algorytmy.wstep;

public class zad9 {
    //Napisz metodę, która zwraca tablice elementów środkowych.
    //makeMiddle([1,2,3,4]) = [2, 3]
    //Zakładamy, że długość tablicy jest zawsze podzielna przez 2.

    public static void main(String[] args) {
        int [] makeMiddle = {1,2,3,4};
        int [] nowa_tab = middle(makeMiddle);
        System.out.println(nowa_tab);

    }

    public static int[] middle(int [] tablica_wej){
        int n = tablica_wej.length;
        int [] tablica = {tablica_wej[n/2-1], tablica_wej[n/2]};

        return tablica;

    }

}
