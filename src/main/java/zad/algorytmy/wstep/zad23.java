package zad.algorytmy.wstep;

import java.util.Scanner;

public class zad23 {
    //Napisz funkcję, która stwierdza, czy zadana jako parametr liczba całkowita jest
    //sześcianem pewnej liczby naturalnej.

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        boolean isSquared = true;

        isNumberSquared(a,b,isSquared);

    }

    private static void isNumberSquared(int a,int b,boolean isSquared){
        if(a*a*a==b){
            System.out.println(isSquared);
        }else{
            System.out.println(!isSquared);
        }
    }
}
