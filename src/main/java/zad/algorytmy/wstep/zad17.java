package zad.algorytmy.wstep;

import java.util.Scanner;

public class zad17 {
    //Napisz metodę sprawdzającą czy liczba jest parzysta.

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        isEvenNumber(a);

    }

    private static void isEvenNumber(int a){
        if(a%2==0){
            System.out.println("Liczba jest parzysta.");
        }else{
            System.out.println("Liczba nie jest parzysta.");
        }

    }
}
