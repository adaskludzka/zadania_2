package zad.algorytmy.wstep;

import java.util.Scanner;

public class zad16 {
    //Napisz metodę, która ma trzy parametry formalne a, b, c będące liczbami
    //całkowitymi. Funkcja zwraca prawdę, jeśli zadane liczby są liczbami
    //pitagorejskimi oraz fałsz w przeciwnym wypadku. Liczby pitagorejskie spełniają
    //warunek: a*a+b*b=c*c.

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        boolean numberIsPythagorean = true;
        isNumbersPythagorean(a,b,c,numberIsPythagorean);

    }

    private static void isNumbersPythagorean(int a, int b, int c,boolean numberIsPythagorean){
        if(a*a+b*b==c*c){
            System.out.println(numberIsPythagorean);
        }else{
            System.out.println(!numberIsPythagorean);
        }
    }
}
