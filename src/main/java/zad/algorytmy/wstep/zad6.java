package zad.algorytmy.wstep;

import java.util.Arrays;

public class zad6 {
    //Napisz metodę, która odwraca daną tablicę liczb całkowitych.
    //swap([1,2,3]) = [3,2,1]

    private static int[] swapArray(int[] swap, int indexOne, int indexTwo, int indexThree){

        int a = swap[indexOne];
        swap[indexOne] = swap[indexTwo];
        swap[indexTwo] = a;

        int b = swap[indexTwo];
        swap[indexTwo] = swap[indexThree];
        swap[indexThree] = b;

        int c = swap[indexTwo];
        swap[indexTwo] = swap[indexOne];
        swap[indexOne] = c;

        return swap;





    }


    public static void main(String[] args) {
        int [] swap = {1,2,3};

        System.out.println(Arrays.toString(swapArray(swap,0,1,2)));

    }
}
