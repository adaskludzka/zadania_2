package zad.algorytm;

import java.util.Scanner;

public class zad9 {
    //Wypisz na ekran N wyrazów ciągu fibonacciego
    //0,1,1,2,3,5,8,13,21,34... każdy kolejny wyraz otrzymujemy po przez sumę dwóch poprzednich to jest
    //1 = 0 + 1
    //2 = 1 + 1
    //3 = 2 + 1
    //5 = 3 + 2
    //itd...

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        a[0] = 0;
        a[1] = 1;

        for(int i = 2; i<n; i++){
            a[i] = a[i-1] + a[i-2];

        }

        for(int i = 0;i<n;i++){
            System.out.println(a[i]);
        }


    }
}
