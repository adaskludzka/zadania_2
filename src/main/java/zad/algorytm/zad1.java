package zad.algorytm;

public class zad1 {
    //Zaprogramuj algorytm znajdujący największy element z podanego ciągu liczb
    //
    //int[] numbers = {1, 13, 51322, -513, 0, 53, 100, 555}
    //Największy element to 51322

    public static void main(String[] args) {
        int[] numbers = {1, 13, 51322, -513, 0, 53, 100, 555};

        int max = numbers[0];

        for(int a : numbers){
            if(max<a){
                max = a;
            }
        }
        System.out.println(max);


    }


}
