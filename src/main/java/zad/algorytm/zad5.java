package zad.algorytm;

public class zad5 {
    //Wypisz liczby od 1 do 100 następnie dla liczb podzielnych przez 3 wypisz Fizz dla liczb podzielnych
    //przez 5 wypisz Buzz dla liczb podzielnych przez 3 i przez 5 wypisz FizzBuzz,
    //w przeciwynym razie wypisz wartość liczby
    //
    //1
    //2
    //Fizz
    //4
    //Buzz
    //...
    //13
    //14
    //FizzBuzz
    //16
    public static void main(String[] args) {

        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println("FizzBuzz");
            }else if(i % 5 == 0){
                System.out.println("Buzz");
            }else if(i % 3 == 0){
                System.out.println("Fizz");
            }else{
                System.out.println(i);
            }
        }
    }
}
